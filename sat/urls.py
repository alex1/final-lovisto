"""sat URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import path
from django.views.generic import RedirectView

from lo_visto.views import ResourceDetailView, index, vote, submit_resource, add_comment, user_profile, all_resources, about, logout_view, login_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='index'),
    path('index/', index, name='index'),
    path('vote/<int:resource_id>/<int:like>/', vote, name='vote'),
    path('resource/<int:pk>/', ResourceDetailView.as_view(), name='resource-detail'),
    path('submit-resource/', submit_resource, name='submit_resource'),
    path('add-comment/', add_comment, name='add_comment'),
    path('user-profile/', user_profile, name='user_profile'),
    path('all-resources/', all_resources, name='all_resources'),
    path('about/', about, name='about'),
    path('logout/', logout_view, name='logout'),
    path('login/', login_view, name='login'),
    path("favicon.ico", RedirectView.as_view(url=staticfiles_storage.url("img/favicon.ico")))
]
