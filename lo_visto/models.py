import re
import xml.etree.ElementTree as ET

import requests
from django.contrib.auth.models import User
from django.db import models
from lxml import etree


class Resource(models.Model):
    ORIGIN_AEMET = 0
    ORIGIN_WIKIPEDIA = 1
    ORIGIN_YOUTUBE = 2
    ORIGIN_OTHER = 3

    CHOICES_ORIGIN = (
        (ORIGIN_AEMET, 'AEMET'),
        (ORIGIN_WIKIPEDIA, 'Wikipedia'),
        (ORIGIN_YOUTUBE, 'YouTube'),
        (ORIGIN_OTHER, 'Other')
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    url = models.URLField()
    title = models.CharField(max_length=250)
    date = models.DateTimeField(auto_now=True)
    description = models.TextField(blank=True)
    origin = models.PositiveSmallIntegerField(choices=CHOICES_ORIGIN, default=ORIGIN_OTHER)

    @property
    def positive_votes(self):
        return Vote.objects.filter(
            resource=self,
            is_positive=True
        ).count()

    @property
    def negative_votes(self):
        return Vote.objects.filter(
            resource=self,
            is_positive=False
        ).count()

    @property
    def comments_count(self):
        return Comment.objects.filter(resource=self).count()

    def __str__(self):
        return f'{self.title} ({self.url})'

    @property
    def extended_info(self):
        if self.origin == Resource.ORIGIN_AEMET:
            return self._get_aemet_extended_info()

        elif self.origin == Resource.ORIGIN_YOUTUBE:
            return self._get_youtube_extended_info()

        elif self.origin == Resource.ORIGIN_WIKIPEDIA:
            return self._get_wikipedia_extended_info()

        # El origen es otro
        return self._get_other_extended_info()

    def _get_aemet_extended_info(self):
        info = {}
        id_pattern = r'^.*-id(?P<id>[0-9]+)$'
        m = re.match(id_pattern, self.url)
        if m:
            location_id = m.group('id')

            # Descargar datos
            data_url = f'https://www.aemet.es/xml/municipios/localidad_{location_id}.xml'
            response = requests.get(data_url)
            root = ET.fromstring(response.text)

            info.update({
                'location': root.find('.//nombre').text,
                'province': root.find('.//provincia').text
            })

            ## Llenar listado con las observaciones registradas cada día
            observations = []
            for day_data in root.findall('.//dia'):
                observations.append({
                    'date': day_data.get('fecha'),
                    'max_temp': day_data.find('.//temperatura/maxima').text,
                    'min_temp': day_data.find('.//temperatura/minima').text,
                    'max_sens': day_data.find('.//sens_termica/maxima').text,
                    'min_sens': day_data.find('.//sens_termica/minima').text,
                    'max_hum': day_data.find('.//humedad_relativa/maxima').text,
                    'min_hum': day_data.find('.//humedad_relativa/minima').text
                })
            info['observations'] = observations
            return info

    def _get_youtube_extended_info(self):
        info = {}
        video_pattern = r'^.*/watch\?v=(?P<video>\w+)$'
        m = re.match(video_pattern, self.url)

        if m:
            video = m.group('video')

            # Descargar datos
            data_url = f'https://www.youtube.com/oembed?format=json&url=https://www.youtube.com/watch?v={video}'
            response_json = requests.get(data_url).json()
            info.update({
                'title': response_json.get('title'),
                'author_name': response_json.get('author_name'),
                'video_iframe': response_json.get('html')
            })

        return info

    def _get_wikipedia_extended_info(self):
        info = {}
        article_pattern = r'^.*/wiki/(?P<article>\w+)$'
        m = re.match(article_pattern, self.url)

        if m:
            article = m.group('article')
            info['article'] = article

            # Descargar texto del artículo
            text_url = f'https://es.wikipedia.org/w/api.php?action=query&format=xml&titles={article}&prop=extracts&exintro&explaintext'
            text_response = requests.get(text_url)
            root = ET.fromstring(text_response.text)
            text = root.find('.//extract').text[:400]
            info['text'] = text

            # Descargar imagen del artículo
            image_url = f'https://es.wikipedia.org/w/api.php?action=query&titles={article}&prop=pageimages&format=json&pithumbsize=100'
            image_response = requests.get(image_url).json()
            try:
                for value in image_response['query']['pages'].values():
                    info['image'] = value['thumbnail']['source']
                    break
            except:
                pass

        return info

    def _get_other_extended_info(self):
        info = {}
        response = requests.get(self.url).text
        htmlparser = etree.HTMLParser()

        tree = etree.fromstring(response, htmlparser)
        try:
            info['title'] = tree.xpath(".//meta[@property='og:title']")[0].get('content')
            info['image'] = tree.xpath(".//meta[@property='og:image']")[0].get('content')
        except:
            try:
                title = tree.xpath(".//title")[0].text
                info['title'] = title
            except:
                pass

        return info


class Vote(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    is_positive = models.BooleanField(default=False)


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=True)
    text = models.TextField()
