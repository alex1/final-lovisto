from django.apps import AppConfig


class LoVistoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lo_visto'
