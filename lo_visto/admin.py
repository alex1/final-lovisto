from django.contrib import admin

from lo_visto.models import Resource, Vote, Comment

admin.site.register(Resource)
admin.site.register(Vote)
admin.site.register(Comment)
