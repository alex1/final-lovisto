from django.contrib.auth.models import User, AnonymousUser
from django.test import TestCase, RequestFactory

from lo_visto.models import Resource, Vote, Comment
from lo_visto.views import index, vote, ResourceDetailView, submit_resource, add_comment, user_profile, all_resources, about, logout_view


class LoVistoTestCase(TestCase):
    def setUp(self) -> None:
        self.factory = RequestFactory()

        # Create users
        self.admin = User.objects.create_superuser(
            username='admin',
            password='1234'
        )
        self.user = User.objects.create(
            username='user',
            password='1234'
        )

        # Create resources
        resources = [
            {
                'id': 1,
                'user_id': self.user.id,
                'url': 'https://www.google.es',
                'title': 'Google',
                'description': 'Internet search engine',
                'origin': Resource.ORIGIN_OTHER
            },
            {
                'id': 2,
                'user_id': self.admin.id,
                'url': 'https://es.wikipedia.org/wiki/Manorina_flavigula',
                'title': 'Manorina flavigula',
                'description': 'El mielero goligualdo o manorina de garganta amarilla (Manorina flavigula)3​4​ es una especie de ave paseriforme de la familia Meliphagidae endémica de Australia. Su distintiva rabadilla de color blanco lo hace fácil de observar en el campo y lo distingue de las otras especies de su género',
                'origin': Resource.ORIGIN_WIKIPEDIA
            },
            {
                'id': 3,
                'user_id': self.user.id,
                'url': 'http://www.aemet.es/es/eltiempo/prediccion/municipios/getafe-id28065',
                'title': 'AEMET - Getafe',
                'description': 'El tiempo en Getafe',
                'origin': Resource.ORIGIN_AEMET
            },
            {
                'id': 4,
                'user_id': self.admin.id,
                'url': 'https://www.bing.com',
                'title': 'Bing',
                'description': 'Another Internet search engine',
                'origin': Resource.ORIGIN_OTHER
            },
            {
                'id': 5,
                'user_id': self.user.id,
                'url': 'https://www.youtube.com/watch?v=IfoSqaxJsAM',
                'title': 'Presentación de la asignatura. Back-end y front-end.',
                'description': 'Presentación de la asignatura. Back-end y front-end.',
                'origin': Resource.ORIGIN_YOUTUBE
            },
        ]
        Resource.objects.bulk_create(
            [Resource(**r) for r in resources]
        )

        # Create votes
        votes = [
            {
                'pk': 1,
                'user_id': self.user.id,
                'resource_id': 5,
                'is_positive': False
            },
            {
                'pk': 2,
                'user_id': self.admin.id,
                'resource_id': 1,
                'is_positive': True
            }
        ]
        Vote.objects.bulk_create(
            [Vote(**v) for v in votes]
        )

        # Create comments
        comments = [
            {
                'pk': 1,
                'user_id': self.user.id,
                'resource_id': 1,
                'text': 'Excelente buscador'
            },
            {
                'pk': 2,
                'user_id': self.user.id,
                'resource_id': 4,
                'text': 'Muy bueno!!'
            },
        ]
        Comment.objects.bulk_create(
            [Comment(**c) for c in comments]
        )

    def test_index(self):
        request = self.factory.get('/index/')

        # Check for authenticated user
        request.user = self.user
        response = index(request)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('Tus últimas aportaciones' in response.content.decode())

        # Check for anonymous user
        request.user = AnonymousUser()
        response = index(request)
        self.assertEqual(response.status_code, 200)
        self.assertFalse('Tus últimas aportaciones' in response.content.decode())

    def test_vote(self):
        request = self.factory.get('/vote/1/0/')
        request.user = self.user
        response = vote(request, 1, 0)
        self.assertEqual(response.status_code, 302)

    def test_resource_detail(self):
        request = self.factory.get('/resource/1/')

        # Test for user
        request.user = self.user
        response = ResourceDetailView.as_view()(request, pk=1)
        self.assertEqual(response.status_code, 200)
        response.render()
        self.assertTrue('Google' in response.content.decode())

        # Test for anonymous user
        request.user = AnonymousUser()
        response = ResourceDetailView.as_view()(request, pk=1)
        self.assertEqual(response.status_code, 200)
        response.render()
        self.assertTrue('Google' in response.content.decode())

    def test_submit_resource(self):
        request = self.factory.post('/submit-resource/', {
            'title': 'Test Resource',
            'url': 'http://test.com',
            'description': 'This resource is not real'
        })
        request.user = self.user
        response = submit_resource(request)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(Resource.objects.filter(title='Test Resource').exists())

    def test_add_comment(self):
        request = self.factory.post('/add-comment/', {
            'resource': 1,
            'text': 'Test comment'
        })
        request.user = self.user
        response = add_comment(request)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(Comment.objects.filter(text='Test comment').exists())

    def test_user_profile(self):
        request = self.factory.get('/user-profile/')
        request.user = self.user
        response = user_profile(request)
        self.assertEqual(response.status_code, 200)

    def test_all_resources(self):
        request = self.factory.get('/all-resources/')

        # Test for logged user
        request.user = self.user
        response = all_resources(request)
        self.assertEqual(response.status_code, 200)

        # Test for anonymous user
        request.user = AnonymousUser
        response = all_resources(request)
        self.assertEqual(response.status_code, 200)

    def test_about(self):
        request = self.factory.get('/about/')

        # Test for logged user
        request.user = self.user
        response = about(request)
        self.assertEqual(response.status_code, 200)

        # Test for anonymous user
        request.user = AnonymousUser
        response = about(request)
        self.assertEqual(response.status_code, 200)
