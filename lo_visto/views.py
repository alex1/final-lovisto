from django.contrib.auth import logout, login, authenticate
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.core import serializers

from django.views.generic import DetailView

from lo_visto.models import Resource, Vote, Comment


def _get_common_context():
    try:
        last_links = Resource.objects.order_by('-date')[:3]
        return {
            'link1': last_links[0],
            'link2': last_links[1],
            'link3': last_links[2],
        }
    except:
        return {}


def _download_resources(download_format, content_type):
    if download_format.lower() not in ('json', 'xml'):
        return HttpResponse(status=404)
    if content_type.lower() not in ('resources', 'comments'):
        return HttpResponse(status=404)

    model = {
        'resources': Resource,
        'comments': Comment
    }[content_type.lower()]

    data = serializers.serialize(download_format, model.objects.all())
    return HttpResponse(data, content_type=f'text/{download_format}')


def index(request):
    download_format = request.GET.get('format')
    if download_format:
        content_type = request.GET.get('type')
        return _download_resources(download_format, content_type)

    resources = Resource.objects.order_by('-date')[:10]
    context = _get_common_context()
    context.update({'resources': resources})

    if request.user.is_authenticated:
        context['liked'] = Vote.objects.filter(
            user=request.user,
            resource__in=resources,
            is_positive=True
        ).values_list('resource__id', flat=True)

        context['disliked'] = Vote.objects.filter(
            user=request.user,
            resource__in=resources,
            is_positive=False
        ).values_list('resource__id', flat=True)

        context['my_resources'] = Resource.objects.filter(user=request.user).order_by('-date')[:5]

    return render(request, 'index.html', context)


def all_resources(request):
    context = _get_common_context()
    context.update({
        'resources': Resource.objects.order_by('-date')
    })
    return render(request, 'all_resources.html', context)


def about(request):
    context = _get_common_context()
    return render(request, 'about.html', context)


@login_required
def vote(request, resource_id, like):
    is_positive = bool(like)

    vote, _ = Vote.objects.get_or_create(
        user=request.user,
        resource_id=resource_id,
        defaults={
            'is_positive': is_positive
        }
    )
    vote.is_positive = is_positive
    vote.save()

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@login_required
def submit_resource(request):
    try:
        resource = Resource.objects.create(
            user=request.user,
            title=request.POST.get('title'),
            description=request.POST.get('description'),
            url=request.POST.get('url')
        )
        if 'wikipedia' in resource.url:
            resource.origin = Resource.ORIGIN_WIKIPEDIA
        elif 'aemet' in resource.url:
            resource.origin = Resource.ORIGIN_AEMET
        elif 'youtube' in resource.url:
            resource.origin = Resource.ORIGIN_YOUTUBE

        resource.save()
        return redirect('index')
    except:
        return HttpResponse(status=400)


@login_required
def add_comment(request):
    try:
        comment = Comment.objects.create(
            user=request.user,
            resource_id=request.POST.get('resource'),
            text=request.POST.get('text')
        )
        return redirect('resource-detail', comment.resource_id)
    except Exception as e:
        return HttpResponse(status=400)


@login_required
def user_profile(request):
    context = _get_common_context()
    context.update({
        'resources': Resource.objects.filter(user=request.user).order_by('-date'),
        'comments': Comment.objects.filter(user=request.user).order_by('-date'),
        'votes': Vote.objects.filter(user=request.user)
    })
    return render(request, 'user_profile.html', context)


@login_required
def logout_view(request):
    logout(request)
    return redirect('index')


def login_view(request):
    if request.method == 'GET':
        return render(request, 'login.html')
    else:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return redirect('index')
        return HttpResponse(status=401)


class ResourceDetailView(DetailView):
    model = Resource
    template_name = 'resource_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(_get_common_context())
        resource = context['object']
        context.update({
            'extended_info': resource.extended_info,
            'comments': resource.comment_set.order_by('-date'),
            'liked': Vote.objects.filter(resource=resource, is_positive=True).values_list('resource_id', flat=True),
            'disliked': Vote.objects.filter(resource=resource, is_positive=False).values_list('resource_id', flat=True)
        })
        return context
